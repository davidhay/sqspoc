require 'logging'
require_relative './env_support'

module SailthruAdapter

  class SqsConfig
    attr_reader :queue_url,:destination

    def createClient
      fail "Not Implemented."
    end
  end

  class AwsSqsConfig < SqsConfig
    include EnvSupport

    AMZ_SQS_QUEUE_URL = 'AMZ_SQS_QUEUE_URL'
    AMZ_REGION = 'AMZ_REGION'
    AMZ_ACCESS_KEY_ID = 'AMZ_ACCESS_KEY_ID'
    AMZ_SECRET_ACCESS_KEY = 'AMZ_SECRET_ACCESS_KEY'

    def initialize
      @queue_url = getEnv(AMZ_SQS_QUEUE_URL)
      @destination = @queue_url
      @aws_region = getEnv(AMZ_REGION)
      @aws_access_key_id = getEnv(AMZ_ACCESS_KEY_ID)
      @aws_secret_access_key = getEnv(AMZ_SECRET_ACCESS_KEY)

      Aws.config.update(
          access_key_id: @aws_access_key_id,
          secret_access_key: @aws_secret_access_key,
          region: @aws_region
      )
    end

    def createClient
      Aws::SQS::Client.new()
    end

    def to_s
      "access_key_id[#{@aws_access_key_id}] secret_access_key[#{@aws_secret_access_key}] region[#{@region}] queue_url[#{@queue_url}]"
    end
  end


end
