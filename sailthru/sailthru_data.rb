require_relative 'template'
require_relative 'sqs_data'

module SailthruAdapter

  class SailthruData
    include Template

    attr_reader :email_address, :template_id, :template_vars, :email_type

    def initialize(email_type, email_address, vars)
      @email_type = email_type
      @template_id = Template.getTemplate(email_type)
      @email_address = email_address
      @template_vars = vars
    end


    def self.getEmailAddress(hash)
      hash[SqsData::SQS_EMAIL_ADDRESS]
    end

    def self.getEmailType(hash)
      hash[SqsData::SQS_EMAIL_TYPE]
    end

    def self.getData(hash)
      fail "wrong type. Exected Hash got #{hash.class}" unless hash.instance_of? Hash
      email_type = getEmailType(hash)
      case email_type
        when TEMP01_NEW_USER_SIGN_UP then
          Data01NewUserSignUp.new(hash)
        when TEMP02_FORGOTTEN_PASSWORD then
          Data02ForgottenPassword.new(hash)
        when TEMP03_PURCHASE_PREMIUM then
          Data03PurchasePremium.new(hash)
        when TEMP04_RENEW_PREMIUM then
          Data04RenewPremium.new(hash)
        when TEMP05_CANCEL_PREMIUM then
          Data05CancelPremium.new(hash)
        when TEMP06_CANCEL_TRIAL then
          Data06CancelTrial.new(hash)
        when TEMP07_UPDATE_ACCOUNT_PROFILE then
          Data07UpdateAccountProfile.new(hash)
        when TEMP08_REGULAR_PAYMENT_FAILED then
          Data08RegularPaymentFailed.new(hash)
        else
          fail "unexpected email_type [#{email_type}]"
      end
    end
  end

  class Data01NewUserSignUp < SailthruData
    def initialize(hash)
      vars = {}
      super(TEMP01_NEW_USER_SIGN_UP, SailthruData.getEmailAddress(hash), vars)
    end
  end
  class Data02ForgottenPassword < SailthruData
    def initialize(hash)
      vars = {}
      super(TEMP02_FORGOTTEN_PASSWORD, SailthruData.getEmailAddress(hash), vars)
    end
  end
  class Data03PurchasePremium < SailthruData
    def initialize(hash)
      vars = {}
      super(TEMP03_PURCHASE_PREMIUM, SailthruData.getEmailAddress(hash), vars)
    end
  end
  class Data04RenewPremium < SailthruData
    def initialize(hash)
      vars = {}
      super(TEMP04_RENEW_PREMIUM, SailthruData.getEmailAddress(hash), vars)
    end
  end
  class Data05CancelPremium < SailthruData
    def initialize(hash)
      vars = {}
      super(TEMP05_CANCEL_PREMIUM, SailthruData.getEmailAddress(hash), vars)
    end
  end
  class Data06CancelTrial < SailthruData
    def initialize(hash)
      vars = {}
      super(TEMP06_CANCEL_TRIAL, SailthruData.getEmailAddress(hash), vars)
    end
  end
  class Data07UpdateAccountProfile < SailthruData
    def initialize(hash)
      vars = {}
      super(TEMP07_UPDATE_ACCOUNT_PROFILE, SailthruData.getEmailAddress(hash), vars)
    end
  end
  class Data08RegularPaymentFailed < SailthruData
    def initialize(hash)
      vars = {}
      super(TEMP08_REGULAR_PAYMENT_FAILED, SailthruData.getEmailAddress(hash), vars)
    end
  end
end

