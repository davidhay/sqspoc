require_relative 'logging'
require_relative 'env_support'
module SailthruAdapter

  class ConfigSailthru
    attr_reader :api_url, :api_key, :api_secret

    def to_s
      "api_url[#{@api_url}] api_key[#{@api_key}] api_secret[#{@api_secret}]"
    end
  end

  class ConfigSailthruEnv < ConfigSailthru
    include EnvSupport

    SAILTHRU_API_URL = "SAILTHRU_URL"
    SAILTHRU_API_KEY = "SAILTHRU_KEY"
    SAILTHRU_API_SECRET = "SAILTHRU_SECRET"

    def initialize
      @api_url = getEnv(SAILTHRU_API_URL)
      @api_key = getEnv(SAILTHRU_API_KEY)
      @api_secret = getEnv(SAILTHRU_API_SECRET)
    end

  end
end
