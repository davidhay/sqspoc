require 'yaml' #ruby library

module SailthruAdapter
  module ConfigSupport

    class << self
      attr_accessor :config
    end
    path=File.join(__dir__, '../resources/config.yaml')
    yaml = File.read(path)
    self.config = YAML.load(yaml)
  end

end