require_relative 'sqs_recvr'

module SailthruAdapter
  class Adapter

      attr_reader :running, :finished

      def initialize(sailthruClient, sqsConfig)
        @sqsRecvr = SqsRecvr.new(sqsConfig.queue_url, sailthruClient)
        @running = false
        @finished = false
      end

      def start
        fail "Already Finished" if @finished
        fail "Already Running" if @running
        stop_polling = -> { @finished }
        @t1 = @sqsRecvr.recv(stop_polling)
        @running=true
      end

      def stop
        fail "Already Finished" if @finished
        fail "Not Running" if !@running
        @finished = true
        @t1.join()
      end
  end
end