require_relative 'sailthru_adapter'
require_relative 'config_sqs'
require_relative 'config_sailthru'
module SailthruAdapter
  class AdapterRunner

      def initialize()
        sqsConfig = AwsSqsConfig.new
        sailthruClient = EnvSailthruConfig.new

        adapter = Adapter.new(sqsConfig, sailthruClient)
        adapter.start()
      end
  end
end