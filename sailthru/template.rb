require_relative 'config_support'

module SailthruAdapter
  module Template

    TEMP01_NEW_USER_SIGN_UP = "newUserSignUp"
    TEMP02_FORGOTTEN_PASSWORD = "forgottenPassword"
    TEMP03_PURCHASE_PREMIUM = "purchasePremium"
    TEMP04_RENEW_PREMIUM = "renewPremium"
    TEMP05_CANCEL_PREMIUM = "cancelPremium"
    TEMP06_CANCEL_TRIAL = "cancelTrial"
    TEMP07_UPDATE_ACCOUNT_PROFILE = "updateAccountProfile"
    TEMP08_REGULAR_PAYMENT_FAILED = "regularPaymentFailed"

    def self.getTemplate(email_type)
      templates = ConfigSupport.config['template_ids']
      template = templates[email_type]
      template
    end

  end
end