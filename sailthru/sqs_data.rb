require 'aws-sdk' #3rd party
require 'bigdecimal' #ruby library
require_relative 'logging'

module SailthruAdapter

  class SqsData
    include Logging

    SQS_EMAIL_ADDRESS = "email_address"
    SQS_EMAIL_TYPE = "email_type"
    SQS_MSG_ID = "$sqs-msg-id"

    def self.getMessageData(msg)
      fail InvalidArgument "Expected Aws::SQS::Types::Message" unless msg.instance_of? Aws::SQS::Types::Message
      result = {}
      msg.message_attributes.each do |attr_name, attr_value|
        if ['String', 'Number'].include? attr_value.data_type
          key = attr_name
          if attr_value.data_type == 'String'
            value = attr_value.string_value
          else
            value = to_numeric(attr_value.string_value)
          end
          result[key]=value
        else
          log.info("message attribute [#{attr_name}] unexpected type [#{attr_value.data_type}] [#{attr_value}]")
        end
      end
      result
    end

    def self.fromMessageData(hash)
      throw InvalidArgument("Expected Hash") unless hash.instance_of? Hash
      msg = Aws::SQS::Types::Message.new
      msg.message_attributes = {}
      hash.each_pair do |key, value|
        puts "KEY #{key} VALUE #{value}"
        attr_value = (value.kind_of? (Numeric)) ? getNumericAttrValue(value) : getStringAttrValue(value)
        msg.message_attributes[key]=attr_value
      end
      msg
    end

    private

    def self.getStringAttrValue(value)
      result = Aws::SQS::Types::MessageAttributeValue.new
      result.string_value=value.to_s
      result.data_type='String'
      result
    end

    def self.getNumericAttrValue(value)
      result = Aws::SQS::Types::MessageAttributeValue.new
      result.string_value=value.to_s
      result.data_type='Number'
      result
    end

    def self.to_numeric(anything)
      num = BigDecimal.new(anything.to_s)
      var = if num.frac == 0
              num.to_i
            else
              num.to_f
            end
      var
    end

  end
end