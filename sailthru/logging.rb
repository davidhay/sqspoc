require 'logging'
require 'tmpdir'

module Logging

  #NOTE : on a mac - the system tmpdir may not be /tmp - to find out what it is $ echo $TMPDIR
  @@logFile=File.join(Dir.tmpdir, 'sailthruAdapter.log')
  puts "USING LOGFILE #{@@logFile}"

  def self.logFile
    @@logFile
  end

  def logger
    @logger ||= Logging.logger_for(self.class.name)
  end

  # Use a hash class-ivar to cache a unique Logger per class:
  @loggers = {}

  class << self

    def logger_for(classname)
      @loggers[classname] ||= configure_logger_for(classname)
    end

    def configure_logger_for(classname)
      myLayout = Logging.layouts.pattern(:pattern => "%d %5l %c - %m\n")

      appenderStdout = Logging.appenders.stdout \
        :level => :debug, \
        :layout => Logging.layouts.pattern(:pattern => "%d %5l %c - %m\n")

      appenderRolling = Logging.appenders.rolling_file @@logFile, \
        :age => 'daily', \
        :layout => myLayout

      log = Logging.logger[classname]
      log.level = :debug #by default
      log.add_appenders appenderStdout, appenderRolling
      log
    end
  end
end

