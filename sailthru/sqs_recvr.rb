require_relative 'sqs_data'
require 'date' #ruby library

module SailthruAdapter
  class SqsRecvr
    include Logging
    attr_reader :sailthru_client, :queue_url

    def initialize (queue_url, sailthru_client)
      fail "sailthru_client parameter cannot be nil" if sailthru_client.nil?
      fail "queue_url parameter cannot be nil or empty" if queue_url.nil? || queue_url.strip.length==0
      @sailthru_client = sailthru_client
      @queue_url = queue_url

      puts "SAILTHRU RECVR initialized with queue_url of #{@queue_url}"
    end

    protected

    def process_msg(msg)
      hash = SqsData.getMessageData(msg)
      logger.info("Processing Sqs Msg [#{msg}]")
      sailthruData = SailthruData.getData(hash)

      template_name = sailthruData.template_id
      email = sailthruData.email_address
      vars = sailthruData.template_vars
      vars[SqsData::SQS_MSG_ID]=msg.message_id
      options = {'test' => 1}
      begin
        response = @sailthru_client.send_email(template_name, email, vars, options)
        logger.info("Sailthru response is [#{response}]")
      rescue Exception => e
        logger.error "Sailthru Client API : send_email failed [#{e.message}]"+"\n"+e.backtrace.join("\n")
      end
    end

    public

    def recv (stop_polling)
      poller = Aws::SQS::QueuePoller.new(@queue_url, {})
      poller.before_request do |stats|
        if stop_polling.call
          logger.warn("Requested to stop polling.")
          throw :stop_polling
        else
          statsMsg = "#{stats.polling_started_at} #{stats.polling_started_at}"
          puts "STATS #{stats.polling_started_at}"
        end
      end
      Thread.new {
        #this is more efficent than getting 1 at 1 time
        poller.poll(wait_time_seconds: 5, max_number_of_messages: 5) do |msgs|
          msgs.each do |msg|
            begin
              process_msg(msg)
              poller.delete_message(msg)
            rescue Exception => e
              #log one error which includes stack traces
              logger.error e.message+"\n"+e.backtrace.join("\n")
            end
          end #msgs.each loop
        end #poller.poll loop
      }
    end

  end

end


