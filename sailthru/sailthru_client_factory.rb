require_relative 'config_sailthru'
require_relative 'logging'
require 'sailthru-client' #3rd party

module SailthruAdapter
  class SailthruClientFactory
    include Logging

    attr_reader :config

    def initialize(sailthru_config)
      @config = sailthru_config
    end

    def createSailthruClient
      api_url = @config.api_url
      api_key = @config.api_key
      api_secret = @config.api_secret

      client = Sailthru::Client.new(api_key, api_secret, api_url, nil, nil,
                                    {:http_read_timeout => 30,
                                     :http_ssl_timeout => 30,
                                     :http_open_timeout => 30})
      logger.info("Initialised Sailthru Client [#{client}] for [#{@config.api_url}]")
      client
    end

  end
end



