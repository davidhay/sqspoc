require 'spec_helper'
require 'ostruct'
require 'aws-sdk'
require 'fake_sqs/test_integration'
require 'rspec/core'
require 'rspec/expectations'
require 'rspec/mocks'
require 'test/unit'
require_relative './my_recvr'

TEST_QUEUE = 'test-queue'
LIMIT=10
def createQueue(queueName)
    resp = $client.create_queue({queue_name: TEST_QUEUE}).to_h
    queueURL = resp[:queue_url]
end
def sendMessageToQueue(queueURL, msgBody)
  msg = {
      queue_url: queueURL, # required
      message_body: msgBody
  }
  resp = $client.send_message(msg)
end

RSpec.describe "MyRecvr", :sqs do
  it "should receive messages that are sent to its queue, process them and stop when requested" do
    queueURL = createQueue(TEST_QUEUE)

    stop = false;
    stop_polling = -> { stop }
    actual = []

    on_mesg = -> (msg) {
      #puts "Recvd message #{msg.body}";
      actual.push(msg) }

    t1 = MyRecvr.recv(queueURL, on_mesg, stop_polling)

    #puts "QUEUE_URL is  #{queueURL}"
    mesgs = (1..LIMIT).collect { |n| "This is test message #{n}" }
    mesgs.each {|msg| sendMessageToQueue(queueURL,msg) }

    sleep(1) unless mesgs.size >= LIMIT
    stop=true

    expect(mesgs.size).to   eq(LIMIT)
    mesgs.each { |msg| expect(mesgs).to include(msg) }
    t1.join
    t1.exit
  end
end

