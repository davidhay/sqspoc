#!/usr/bin/bash

function checkEnvVariable {
    name=$1
    value=`printenv $name`
    if [[ -z $value ]]; then
        echo "The value of [$1] is NOT DEFINED"
    else
        echo "The value of [$1] is : '$value'"
    fi
}

checkEnvVariable AMZ_REGION
checkEnvVariable AMZ_ACCESS_KEY_ID
checkEnvVariable AMZ_SECRET_ACCESS_KEY
checkEnvVariable AMZ_SQS_QUEUE_URL


