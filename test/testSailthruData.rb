require_relative '../sailthru/template'
require_relative '../sailthru/sailthru_data'
require "test/unit"

module SailthruAdapter

  #tests conversion of hash of name/value pairs into subclass instance of SailthruAdapter::SailthruData

  class TestSailthruData < Test::Unit::TestCase
    include Template

    def setup
      @hash = {SqsData::SQS_EMAIL_ADDRESS=>"test@test.com", key1: "value1", key2: "value2"}
    end

    def testTemplates01NewUserSignUp
      data = checkBasicTemplateDataForEmailType(TEMP01_NEW_USER_SIGN_UP)
    end

    def testTemplate02ForgottenPassword
      data = checkBasicTemplateDataForEmailType(TEMP02_FORGOTTEN_PASSWORD)
    end

    def testTemplpate03PurchasePremium
      data = checkBasicTemplateDataForEmailType(TEMP03_PURCHASE_PREMIUM)
    end

    def testTemplate04RenewPremium
      data = checkBasicTemplateDataForEmailType(TEMP04_RENEW_PREMIUM)
    end

    def testTemplate05CancelPremium
      data = checkBasicTemplateDataForEmailType(TEMP05_CANCEL_PREMIUM)
    end

    def testTemplate06CancelTrial
      data = checkBasicTemplateDataForEmailType(TEMP06_CANCEL_TRIAL)
    end

    def testTemplate07UpdateAccountProfile
      data = checkBasicTemplateDataForEmailType(TEMP07_UPDATE_ACCOUNT_PROFILE)
    end

    def testTemplate08RegularPaymentFailed
      data = checkBasicTemplateDataForEmailType(TEMP08_REGULAR_PAYMENT_FAILED)
    end

    protected

    def checkBasicTemplateDataForEmailType(email_type)
      extra = { SqsData::SQS_EMAIL_TYPE => email_type}
      hash = @hash.merge extra
      data = SailthruData.getData(hash)
      assert_equal(email_type, data.email_type)
      assert_equal("template_#{email_type}", data.template_id)
      expectedEnd = upcaseFirstLetter(email_type)
      assert_true(data.class.name.end_with? expectedEnd, "unexpected class name")
      data
    end

    private

    def upcaseFirstLetter(word)
      letters = word.split('')
      letters.first.upcase!
      letters.join
    end

  end

end
