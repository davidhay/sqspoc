require_relative '../sailthru/template'
require_relative '../sailthru/sailthru_data'
require_relative '../sailthru/sqs_data'
require "test/unit"
require 'aws-sdk'
require 'securerandom'

module SailthruAdapter
  class TestSqsUtils < Test::Unit::TestCase
    include Template

    def testTemplates01NewUserSignUp
      checkMessageData(TEMP01_NEW_USER_SIGN_UP, "user1@test.com")
    end

    def testTemplate02ForgottenPassword
      checkMessageData(TEMP02_FORGOTTEN_PASSWORD, "user2@test.com")
    end

    def testTemplpate03PurchasePremium
      checkMessageData(TEMP03_PURCHASE_PREMIUM, "user3@test.com")
    end

    def testTemplate04RenewPremium
      checkMessageData(TEMP04_RENEW_PREMIUM, "user4@test.com")
    end

    def testTemplate05CancelPremium
      checkMessageData(TEMP05_CANCEL_PREMIUM, "user5@test.com")
    end

    def testTemplate06CancelTrial
      checkMessageData(TEMP06_CANCEL_TRIAL, "user6@test.com")
    end

    def testTemplate07UpdateAccountProfile
      checkMessageData(TEMP07_UPDATE_ACCOUNT_PROFILE, "user7@test.com")
    end

    def testTemplate08RegularPaymentFailed
      checkMessageData(TEMP08_REGULAR_PAYMENT_FAILED, "user8@test.com")
    end

    private

    def checkMessageData(email_type, email_address, others={key1: SecureRandom.uuid, key2: SecureRandom.uuid})

      hash = {}
      hash.merge!(others)
      hash.merge!({SqsData::SQS_EMAIL_TYPE => email_type, SqsData::SQS_EMAIL_ADDRESS => email_address })

      msg = SqsData.fromMessageData(hash)
      data = SqsData.getMessageData(msg)

      assert_equal(email_type, data[SqsData::SQS_EMAIL_TYPE])
      assert_equal(email_address, data[SqsData::SQS_EMAIL_ADDRESS])

      others.each_pair do |key, value|
        assert_equal(value, data[key])
      end
    end
  end
end

