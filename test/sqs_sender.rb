require_relative '../sailthru/sqs_data'

module SailthruAdapter

  # used to send messages to SQS for tests
  class SqsSender
    include Logging

    attr_reader :sqsClient, :queue_url

    def initialize(sqsConfig)
      if sqsConfig.respond_to? "start"
        sqsConfig.start
      end
      @queue_url = sqsConfig.queue_url
      @sqsClient = sqsConfig.createClient
    end

    def sendMessage(msg)
      fail "cannot send message. Expected Hash got #{msg.class}" unless msg.instance_of? Hash
      fail "queue mismatch" if msg[:queue_url] != @queue_url
      resp = sqsClient.send_message(msg)
      msgId = resp.message_id
      result = Hash.new
      result[msgId]=msg
      result
    end

    def sendMessages(msgs)
      sentMessages = {}
      msgs.each do |msg|
        sentMessage = sendMessage(msg)
        sentMessages.merge!(sentMessage)
      end
      sentMessages
    end

    private

    def getMessageToSend(email_type, email_address, vars={})
      attributes = {}
      vars.each_pair do |k, v|
        addMessageAttribute(attributes, k, v)
      end
      addMessageAttribute(attributes, SqsData::SQS_EMAIL_TYPE, email_type);
      addMessageAttribute(attributes, SqsData::SQS_EMAIL_ADDRESS, email_address);
      {
          queue_url: queue_url,
          message_body: "because I have to", # required
          delay_seconds: 0,
          message_attributes: attributes,
      }
    end

    def addMessageAttribute(hash, name, value)
      hash[name]= {
          string_value: value,
          data_type: "String", # required
      }
    end

    public

    def createTestMessages
      vars = {}
      msgsToSend = []
      msgsToSend << getMessageToSend(Template::TEMP01_NEW_USER_SIGN_UP, "test01@test.com", vars)
      msgsToSend << getMessageToSend(Template::TEMP02_FORGOTTEN_PASSWORD, "test02@test.com", vars)
      msgsToSend << getMessageToSend(Template::TEMP03_PURCHASE_PREMIUM, "test03@test.com", vars)
      msgsToSend << getMessageToSend(Template::TEMP04_RENEW_PREMIUM, "test04@test.com", vars)
      msgsToSend << getMessageToSend(Template::TEMP05_CANCEL_PREMIUM, "test05@test.com", vars)
      msgsToSend << getMessageToSend(Template::TEMP06_CANCEL_TRIAL, "test06@test.com", vars)
      msgsToSend << getMessageToSend(Template::TEMP07_UPDATE_ACCOUNT_PROFILE, "test07@test.com", vars)
      msgsToSend << getMessageToSend(Template::TEMP08_REGULAR_PAYMENT_FAILED, "test08@test.com", vars)
      msgsToSend
    end

  end
end