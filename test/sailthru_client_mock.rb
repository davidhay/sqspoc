require 'delegate'
module SailthruAdapter

  # this class is used during testing instead of actual sailthru endpoint
  class SailthruClientMock < SimpleDelegator
    include Logging

    STHRU_TEMPLATE = 'template_name'
    STHRU_EMAIL = 'email'
    STHRU_VARS = 'vars'
    STHRU_OPTIONS = 'options'
    STHRU_SCHED_TIME = 'schedule_time'

    attr_reader :sailthru_requests
    attr_accessor :response_generator

    def initialize(target)
      fail 'The instance to decorate must be a SailthruClient!' unless target.instance_of? Sailthru::Client
      super(target)
      @sailthru_requests = []
    end

    # instead of actually sending the email to sailthru, we record the messages
    def send_email(template_name, email, vars=nil, options=nil, schedule_time=nil)
      "template_name[#{template_name}] email[#{email}] vars[#{vars}] options[#{options}] schedule_time[#{schedule_time}]"

      hash = {}
      hash[STHRU_TEMPLATE]=template_name
      hash[STHRU_EMAIL]=email
      hash[STHRU_VARS]=vars
      hash[STHRU_OPTIONS]=options
      hash[STHRU_SCHED_TIME]=schedule_time

      @sailthru_requests << hash
      "mock response for template_name[#{template_name}] email[#{email}]"
    end
  end
end
