require_relative '../sailthru/template'
require_relative '../sailthru/sailthru_data'
require_relative '../sailthru/sqs_data'
require "test/unit"
require 'aws-sdk'

module SailthruAdapter

  # tests conversion SqsMessage into subclass instance of SailthruAdapter::SailthruData

  class TestSailthruDataFromSqsMessage < Test::Unit::TestCase
    include Template

    def testTemplates01NewUserSignUp
      data = checkBasicTemplateDataForEmailType(TEMP01_NEW_USER_SIGN_UP)
    end

    def testTemplate02ForgottenPassword
      data = checkBasicTemplateDataForEmailType(TEMP02_FORGOTTEN_PASSWORD)
    end

    def testTemplpate03PurchasePremium
      data = checkBasicTemplateDataForEmailType(TEMP03_PURCHASE_PREMIUM)
    end

    def testTemplate04RenewPremium
      data = checkBasicTemplateDataForEmailType(TEMP04_RENEW_PREMIUM)
    end

    def testTemplate05CancelPremium
      data = checkBasicTemplateDataForEmailType(TEMP05_CANCEL_PREMIUM)
    end

    def testTemplate06CancelTrial
      data = checkBasicTemplateDataForEmailType(TEMP06_CANCEL_TRIAL)
    end

    def testTemplate07UpdateAccountProfile
      data = checkBasicTemplateDataForEmailType(TEMP07_UPDATE_ACCOUNT_PROFILE)
    end

    def testTemplate08RegularPaymentFailed
      data = checkBasicTemplateDataForEmailType(TEMP08_REGULAR_PAYMENT_FAILED)
    end


    protected

    def checkBasicTemplateDataForEmailType(email_type)
      email_address = "#{email_type}@test.com"

      hash = {
          SqsData::SQS_EMAIL_TYPE => email_type,
          SqsData::SQS_EMAIL_ADDRESS => email_address
      }
      msg = SqsData.fromMessageData(hash) #convert hash into SQS MSG
      hash2 = SqsData.getMessageData(msg) #part1 get hash from SQS MSG
      data = SailthruData.getData(hash2) #part2 getSailthruData from hash
      assert_equal(email_type, data.email_type)
      assert_equal("template_#{email_type}", data.template_id)
      expectedEnd = upcaseFirstLetter(email_type)
      assert_true(data.class.name.end_with? expectedEnd, "unexpected class name")
      assert_true(data.kind_of? SailthruData)
      data
    end

    def upcaseFirstLetter(word)
      letters = word.split('')
      letters.first.upcase!
      letters.join
    end

  end

end
