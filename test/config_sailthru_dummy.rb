require_relative '../sailthru/config_sailthru'

module SailthruAdapter

  class ConfigSailthruDummy < ConfigSailthru
    include EnvSupport

    def initialize
      @api_url = "dummy_api_url"
      @api_key = "dummy_api_key"
      @api_secret = "dummy_api_secret"
    end
  end

end
