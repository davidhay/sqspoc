require_relative '../sailthru/logging'
require 'test-unit'

class TestLogging < Test::Unit::TestCase

  #tests that we can log lines to an actual log file
  class ClassThatLogs
    include Logging

    def initialize
      logger.level = :info
    end

    def method1
      logger.info("logging a call to method 1")
    end

    def method2
      logger.info("logging a call to method 2")
    end
  end

  private_constant :ClassThatLogs

  def getFileSize(file)
    File.readlines(file).size
  rescue Exception => e
    0
  end

  def testLogging

    beforeCount = getFileSize(Logging.logFile)
    obj1 = ClassThatLogs.new
    obj1.method1
    obj1.method2
    afterCount = getFileSize(Logging.logFile)
    assert_equal(2+beforeCount, afterCount, "Expected 2 more log lines")
  end
end

