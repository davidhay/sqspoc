require 'ostruct'
require 'aws-sdk'
require 'fake_sqs/test_integration'
require_relative '../sailthru/config_sqs'

# this class is a work in progress - looks like FakeSqs does not support message attributes which we need.
module SailthruAdapter
  class SqsConfigFake < SqsConfig
    include Logging

    attr_reader :config, :fake_sqs_server

    def initialize(queue_name='fake-sqs-queue', fakeServerPort=nil)
      port = getFreeLocalPort if fakeServerPort.nil?
      fakeConfig = OpenStruct.new
      fakeConfig.access_key_id='Fake'
      fakeConfig.secret_access_key='Fake'
      fakeConfig.region='Fake'
      fakeConfig.host="localhost"
      fakeConfig.port = port
      @config = fakeConfig
      Aws.config.update(
          access_key_id: @config.access_key_id,
          secret_access_key: @config.secret_access_key,
          region: @config.region
      )
      @fake_sqs_server = FakeSQS::TestIntegration.new(
          database: ":memory:",
          sqs_endpoint: @config.host,
          sqs_port: @config.port
      )
      @aws_region=@config.region
      @destination=@queue_name
      @queue_name=queue_name
      @running=false
      logger.info("Initialised. FakeSqsServer Port [#{@config.port}]. Q Name[#{@queue_name}]")
    end

    def createClient
      fail 'Fake Server not running!' unless @running
      createClientInternal
    end

    def start
      fail 'Already running!' if running?
      @fake_sqs_server.start
      client = createClientInternal
      resp = client.create_queue({queue_name: @queue_name}).to_h
      @queue_url = resp[:queue_url]
    end

    def stop
      fail 'Not running' unless running?
      @fake_sqs_server.stop
      @fake_sqs_server = nil
    end

    def running?
      (!@fake_sqs_server.nil?) && @fake_sqs_server.up?
    end

    def queue_url
      fail 'Not running' unless running?
      @queue_url
    end

    def to_s
      "SqsConfigFake #{@config} #{@fake_sqs_server} #{@running}"
    end

    private

    def createClientInternal
      client = Aws::SQS::Client.new(region: @config.region)
      client.config.endpoint = @fake_sqs_server.uri
      client
    end

    def getFreeLocalPort
      2112
    end

  end
end