require 'aws-sdk'
require 'delegate'
require_relative '../sailthru/config_sqs'
require_relative '../sailthru/config_sailthru'
require_relative '../sailthru/sailthru_client_factory'
require_relative '../sailthru/sailthru_data'
require_relative '../sailthru/sqs_recvr'
require_relative '../sailthru/sqs_data'
require_relative '../sailthru/template'
require_relative '../sailthru/sailthru_adapter'
require_relative 'sqs_sender'
require_relative 'sailthru_client_mock'
require_relative 'config_sailthru_dummy'
require 'test-unit'

module SailthruAdapter

  class TestSailthruAdapter < Test::Unit::TestCase
    include Template

    DURATION = 10
    MOCK=true

    def setup
      # GET SQS CONFIG
      sqsConfig = AwsSqsConfig.new
      puts "SQS_CONFIG #{sqsConfig}"
      @sqsSender = SqsSender.new(sqsConfig)

      # GET SAILTHRU CONFIG
      sailthruConfig = MOCK ? ConfigSailthruDummy.new : ConfigSailthruEnv.new
      puts "SAILTHRU CONFIG IS #{sailthruConfig}"

      # GET SAILTHRU CLIENT
      @sailthruClient = SailthruClientFactory.new(sailthruConfig).createSailthruClient
      if MOCK
        @sailthruClient = SailthruClientMock.new(@sailthruClient)
      end

      puts "USING sailthru client #{@sailthruClient}"
      # CREATE SAILTHRU ADAPTER
      @sailthruAdapter = Adapter.new(@sailthruClient, sqsConfig)
      @sailthruAdapter.start()
    end

    def teardown
      @sailthruAdapter.stop() unless @sailthruAdapter.nil?
    end

    def testSailthruAdapter

      msgsToSend = @sqsSender.createTestMessages

      # SEND MESSAGES
      messagesSent = @sqsSender.sendMessages msgsToSend

      # WAIT UNTIL MESSAGES HAVE BEEN RECVD
      wait(DURATION)

      if MOCK
        sailthruRequests = @sailthruClient.sailthru_requests

        assert_true(messagesSent.size <= sailthruRequests.size)

        messagesSent.each_pair do |sqs_msg_id, msg|
          req=sailthruRequests.find { |req| req[SailthruClientMock::STHRU_VARS][SqsData::SQS_MSG_ID]==sqs_msg_id }
          assert_true(req.nil? == false, "Sailthru mock did not receive request originating from sqs message with id [#{sqs_msg_id}]")

          email_address = msg[:message_attributes][SqsData::SQS_EMAIL_ADDRESS][:string_value]
          email_type = msg[:message_attributes][SqsData::SQS_EMAIL_TYPE][:string_value]

          #CHECK EMAIL ADDRESS
          assert_equal(email_address, req[SailthruClientMock::STHRU_EMAIL])

          #CHECK TEMPLATE is derived from SQS_EMAIL_TYPE
          actual_template_name = req[SailthruClientMock::STHRU_TEMPLATE]
          expected_template_name = Template::getTemplate(email_type)
        end
      end
      puts "fin."
    end

    def wait(duration)
      slept=0
      while slept < duration
        puts "Slept for #{slept} second. #{duration-slept} to go."
        sleep 1
        slept += 1
      end
    end
  end

end