require 'ostruct'
require 'aws-sdk'
require 'fake_sqs/test_integration'


require 'test-unit'

module SailthruAdapter

  module SailthruAdapter
    class TestSqsFakeSendRecv < Test::Unit::TestCase
      LIMIT = 25
      DURATION = 2

      def setup
        config = OpenStruct.new
        config.access_key_id='Fake'
        config.secret_access_key='Fake'
        config.region='Fake'
        config.host="localhost"
        config.port = 1234

        Aws.config.update(
            access_key_id: config.access_key_id,
            secret_access_key: config.secret_access_key,
            region: config.region
        )
        @fake_sqs = FakeSQS::TestIntegration.new(
            database: ":memory:",
            sqs_endpoint: config.host,
            sqs_port: config.port
        )
        @fakeConfig = config
        @fake_sqs.start
      end

      def teardown
        @fake_sqs.stop unless @fake_sqs.nil?
      end

      def testSendRecv
        # creating the server, the client AND the queue
        client = Aws::SQS::Client.new(region: @fakeConfig.region)
        client.config.endpoint = @fake_sqs.uri
        resp = client.create_queue({queue_name: "my-queue"}).to_h
        queueURL = resp[:queue_url]
        puts "QUEUE_URL is  #{queueURL}"

        finish_polling=false
        poller = Aws::SQS::QueuePoller.new(queueURL)
        poller.before_request do |stats|
          #puts("requests: #{stats.request_count}")
          #puts("messages: #{stats.received_message_count}")
          #puts("last-timestamp: #{stats.last_message_received_at}")
          throw :stop_polling if finish_polling
        end

        received = 0
        t1 = Thread.new {

          def shouldProcess(msg)
            puts "in should process #{msg.body}"
            true
          end

          poller.poll(skip_delete: true) do |msg|
            if shouldProcess(msg)
              puts "RECD MESSAGE [#{msg.body}]"
              received += 1
              poller.delete_message(msg)
            else
              #msg will re-appear on queue for others to process
            end
          end
        }

        (1..LIMIT).each do |n|
          msg = {
              queue_url: queueURL, # required
              message_body: "This is message #{n}/#{LIMIT}", # required
          }
          resp = client.send_message(msg)
        end

        slept=0
        while slept < DURATION do
          puts "Slept for #{slept} second. #{DURATION-slept} to go."
          sleep 1
          slept += 1
        end
        finish_polling=true
        t1.join
        assert_equal(LIMIT, received)
        puts "fin."
      end

    end
  end
end
